import React, { useState } from 'react';
import { Alert } from '../components/Alert';
import { Card } from '../components/Card';
import { GMap } from '../components/Map';
import { TabBar } from '../components/TabBar';
import { User } from '../model/user';


const names: User[] = [
  { id: 11, name: 'Japan'},
  { id: 21, name: 'Italy'},
  { id: 32, name: 'Germany'},
];

function UiKitDemo() {
  const [error, setError] = useState<boolean>(false);
  const [activeUser, setActiveUser] = useState<User | null>(names[0])

  function openUrl() {
    window.open('http://www.google.com')
  }

  function doSomething() {
    alert('ciao')
  }

  function tabClickHandler(user: User) {
    setActiveUser(user);
  }
  return (
    <div className="container mt-3">

      <TabBar
        labelField="name"
        data={names}
        activeTab={activeUser}
        onTabClick={tabClickHandler}
      />
      {activeUser && <GMap city={activeUser?.name}/>}

      {error && <Alert type="failed" onClose={() => setError(false)}>AAAAAA</Alert>}
      {error && <Alert type="success" onClose={() => setError(false)}>AAAAAA</Alert>}
      {error && <Alert type="info" onClose={() => setError(false)}>AAAAAA</Alert>}
      <button onClick={() => setError(true)}>Generate Error</button>
      <button onClick={() => setActiveUser(names[2])}>Go to Germany</button>

      <hr/>

      <Card title="Google" icon="fa fa-google"
            onIconClick={openUrl}>
        <button>xyz</button>
        <button>xyz</button>
        <button>xyz</button>
      </Card>

      <Card title="Microsoft" icon="fa fa-windows"
            onIconClick={doSomething}>
        <button>xyz</button>
        <button>xyz</button>
        <button>xyz</button>
      </Card>
    </div>
  );
}

export default UiKitDemo;




