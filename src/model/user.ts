export interface User {
  id: number;
  name: string;
}
export type UserForm = Omit<User, 'id'>;
