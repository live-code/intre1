import React, { useState } from 'react';
import './App.css';

import { User } from './model/user';
import UiKitDemo from './pages/UiKitDemo';
import { TabBar } from './components/TabBar';

const sections: any[] = [
  { id: 1, title: 'uikit' },
  { id: 2, title: 'page2' },
  { id: 3, title: 'page3' }
]
function App() {
  const [ section, setSection ] = useState<any>({ id: 1, title: 'uikit' });
  return (
    <div className="container mt-3">

     <TabBar
        labelField="title"
        data={sections}
        activeTab={section}
        onTabClick={(tab) => setSection(tab)}
      />

      { section.title === 'uikit' && <UiKitDemo/>}
      { section.title === 'page2' &&  <div>pagina 2</div>}
      { section.title === 'page3' &&  <div>pagina 3</div>}


    </div>
  );
}

export default App;




