import React, { useState } from 'react';



interface CardProps {
  title: string;
  icon: string;
  onIconClick: () => void;
}

export const Card: React.FC<CardProps> = props => {
  const { title, children, onIconClick, icon} = props;
  const [isOpen, setIsOpen] = useState<boolean>(true);

  return (
    <div className="card" >
      <div className="card-header" onClick={() => setIsOpen(!isOpen)}>
        {title}

        <div className="pull-right">
          <i className={icon} onClick={onIconClick}/>
        </div>
      </div>
      {
        isOpen && <div className="card-body">
          {children}
        </div>}
    </div>
  )
}

