import React  from 'react';

interface MapProps {
  city: string | null;
}
export const GMap: React.FC<MapProps> = ({ city }) => {
  return   <img
      src={'https://maps.googleapis.com/maps/api/staticmap?center=' + city + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'}
      alt=""/>
};
