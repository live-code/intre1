import React, { useState } from 'react';
import cn from 'classnames';

interface TabBarProps {
  data: any[];
  labelField: string;
  activeTab: any | null;
  onTabClick: (item: any) => void;
}
export const TabBar: React.FC<TabBarProps> = (props) => {
  return (
    <ul className="nav nav-tabs">
      {
        props.data.map(tab => {
          return (
            <li className="nav-item"
                onClick={() => props.onTabClick(tab) }
                key={tab.id}>
              <a className={cn(
                'nav-link', { 'active': tab.id === props.activeTab?.id})}
              >{tab[props.labelField]}</a>
            </li>
          )
        })
      }

    </ul>
  )
};
