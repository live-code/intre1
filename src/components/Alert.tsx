import React  from 'react';
import cn from 'classnames';

interface AlertProps {
  type: 'success' | 'failed' | 'info';
  onClose: () => void;
}

export const Alert: React.FC<AlertProps> = (props) => {
  return <div className={cn(
    'alert', {
      'alert-success': props.type === 'success',
      'alert-danger': props.type === 'failed',
      'alert-info': props.type === 'info',
    } )}
  >
    {props.type === 'failed' && <i className="fa fa-exclamation-triangle mr-3"/>}

    {props.children}

    <button
      onClick={props.onClose}
      type="button" className="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
};
